
{ Autor:   Jürgen Schlottke, Schönaich-C.-Str. 46, D-W 2200 Elmshorn
           Tel. 04121/63109
  Zweck  : Demonstration der Schachprogrammierung unter Turbo-Pascal
  Datum  : irgendwann 1991, als PD freigegeben am 18.01.93
  Version: ohne Versionsnummer
}

unit PlayerCore;

interface

uses
  PlayerTypes;

const
  CListSize = 128;
  
type
  TCastling = (g1, c1, g8, c8);

  TMove = record
    FFr, FTo: shortint;
    FVal: integer;
  end;

  TMoveList = array[1..CListSize] of TMove;

  TChessPosition = class
  strict private
    FBoard: TChessboard;
    FActive: shortint;
    FCastling: array[TCastling] of boolean;
    FEnPassant: array[CBlack..CWhite] of shortint;
    FMoveList: TMoveList;
    FMoveCount: byte;
    FBalance: integer;
    FTwoKings: boolean;
  public
    constructor Create(const AFen: string); overload;
    procedure SetPosition(const AFen: string); overload;
    procedure SetPosition(const APos: TChessPosition); overload;
    function Eval(const AColor: shortint): integer;
    function MovePiece(AFr, ATo: shortint; const APromo: shortint): boolean;
    procedure AppendMove(const AFr, ATo: shortint; const ACondition: boolean = TRUE);
    procedure GenerateSimpleMoves;
    function Check: boolean;
    function CastlingCheck(const AKSquare, AStep: shortint): boolean;
    procedure GenerateCastling(const AKSquare, AStep, ARSquare: shortint);
    procedure GenerateMoves;
    function BestEval(const AColor, ADepth, AAlpha: integer): integer; overload;
    function BestEval(const AAlpha: integer): integer; overload;
    function IsLegal(const AMove: integer): boolean;
    function BoardAsText(const APretty: boolean): string;
    function GetFen: string;
    property ActiveColor: shortint read FActive write FActive;
    property MoveCount: byte read FMoveCount;
    property MoveList: TMoveList read FMoveList;
    property Board: TChessboard read FBoard;
  end;

implementation

uses
  Classes, SysUtils, TypInfo, Settings{$IFDEF DEBUG}, Log{$ENDIF}, Global;

const
{ Vectors }
  CVPawn  : array[1..4] of shortint = ( 01,  02, -09,  11);
  CVBishop: array[1..4] of shortint = ( 11, -11,  09, -09);
  CVRook  : array[1..4] of shortint = (-01,  01, -10,  10);
  CVKnight: array[1..8] of shortint = ( 12,  21,  19,  08, -12, -21, -19, -08);
  CVKing  : array[1..8] of shortint = (-01,  01, -10,  10,  11, -11,  09, -09);

constructor TChessPosition.Create(const AFen: string);
begin
  inherited Create;
  SetPosition(AFen);
end;

procedure TChessPosition.SetPosition(const AFen: string);
var
  LFields: TStringList;
  x, y: integer;
  i, j: integer;
begin
  LFields := TStringList.Create;
  ExtractStrings([' '], [], pchar(AFen), LFields);
  Assert(LFields.Count in [4, 6]);
  FillByte(FBoard, SizeOf(FBoard), CBorder);
  x := 1;
  y := 8;
  for i := 1 to Length(LFields[0]) do
  begin
    case LFields[0][i] of
      '/':
        begin
          x := 1;
          Dec(y);
        end;
      '1'..'8':
        begin
          j := RowToInt(LFields[0][i]);
          while j > 0 do
          begin
            FBoard[10 * x + y] := CNil;
            Inc(x);
            Dec(j);
          end;
        end;
      'P', 'N', 'B', 'R', 'Q', 'K', 'p', 'n', 'b', 'r', 'q', 'k':
        begin
          FBoard[10 * x + y] := PieceToInt(LFields[0][i]);
          Inc(x);
        end;
    end;
  end;

  if LFields[1] = 'w' then
    FActive := CWhite
  else
    FActive := CBlack;

  FCastling[g1] := Pos('K', LFields[2]) > 0;
  FCastling[c1] := Pos('Q', LFields[2]) > 0;
  FCastling[g8] := Pos('k', LFields[2]) > 0;
  FCastling[c8] := Pos('q', LFields[2]) > 0;

  if LFields[3] <> '-' then
    FEnPassant[FActive] := 10 * ColToInt(LFields[3][1]) + RowToInt(LFields[3][2]);
  FEnPassant[CBlack * FActive] := CNil;

  FBalance := 0;
  for x := 1 to 8 do
    for y := 1 to 8 do
      Inc(FBalance, FBoard[10 * x + y]);

  FTwoKings := (Pos('K', LFields[0]) > 0) and (Pos('k', LFields[0]) > 0);

  LFields.Free;
end;

procedure TChessPosition.SetPosition(const APos: TChessPosition);
begin
  FBoard     := APos.FBoard;
  FActive    := APos.FActive;
  FCastling  := APos.FCastling;
  FEnPassant := APos.FEnPassant;
  FMoveList  := APos.FMoveList;
  FMoveCount := APos.FMoveCount;
  FBalance   := APos.FBalance;
  FTwoKings  := APos.FTwoKings;
end;

function TChessPosition.Eval(const AColor: shortint): integer;
begin
  result := FBalance * AColor;
end;

function TChessPosition.MovePiece(AFr, ATo: shortint; const APromo: shortint): boolean;
begin
  result := FALSE;
  if FBoard[AFr] * FActive = CKing then
    case AFr of
      51: begin FCastling[g1] := FALSE; FCastling[c1] := FALSE; end;
      58: begin FCastling[g8] := FALSE; FCastling[c8] := FALSE; end;
    end;

  if FBoard[AFr] * FActive = CRook then
    case AFr of
      81: FCastling[g1] := FALSE;
      11: FCastling[c1] := FALSE;
      88: FCastling[g8] := FALSE;
      18: FCastling[c8] := FALSE;
    end;

  if (Abs(ATo - AFr) = 2) and (FBoard[AFr] * FActive = CPawn) then
    FEnPassant[FActive] := AFr + CVPawn[1] * FActive
  else
    FEnPassant[FActive] := CNil;

  if (ATo = FEnPassant[CBlack * FActive]) and (FBoard[AFr] * FActive = CPawn) then
  begin
    FEnPassant[FActive] := CNil;
    MovePiece(AFr, (ATo div 10) * 10 + AFr mod 10, CQueen);
    FActive := CBlack * FActive;
    AFr := (ATo div 10) * 10 + AFr mod 10;
  end;

  if (AFr in [51, 58]) and (ATo in [71, 31, 78, 38]) and (FBoard[AFr] * FActive = CKing) then
  begin
    FBoard[ATo] := FBoard[AFr];
    FBoard[AFr] := CNil;
    if ATo div 10 = 7 then
    begin
      AFr := AFr mod 10 + 80;
      ATo := AFr - 20;
    end else
    begin
      AFr := AFr mod 10 + 10;
      ATo := AFr + 30;
    end;
    FBoard[ATo] := FBoard[AFr];
    FBoard[AFr] := CNil;
  end else
  begin
    FBalance := FBalance - FBoard[ATo];

    if FBoard[ATo] * CBlack * FActive = CKing then
      FTwoKings := FALSE;

    FBoard[ATo] := FBoard[AFr];
    FBoard[AFr] := CNil;
    if ((ATo mod 10 = 1) or (ATo mod 10 = 8)) and (FBoard[ATo] * FActive = CPawn) then
    begin
      result := TRUE;
      FBoard[ATo] := APromo * FActive;
      FBalance := FBalance + (APromo - CPawn) * FActive;
    end;
  end;

  FActive := CBlack * FActive;
end;

procedure TChessPosition.AppendMove(const AFr, ATo: shortint; const ACondition: boolean = TRUE);
begin
  if not (ACondition and FTwoKings) then
    Exit;
  if FMoveCount < CListSize then
  begin
    Inc(FMoveCount);
    FMoveList[FMoveCount].FFr := AFr;
    FMoveList[FMoveCount].FTo := ATo;
  end;
end;

procedure TChessPosition.GenerateSimpleMoves;
var
  f, t, x, y, i: integer;
begin
  FMoveCount := 0;
  for x := 1 to 8 do
    for y := 1 to 8 do
    begin
      f := 10 * x + y;
      if FBoard[f] <> CNil then
      begin
        case FBoard[f] * FActive of
          CPawn:
            begin
              t := f + CVPawn[1] * FActive;
              if FBoard[t] = CNil then
              begin
                AppendMove(f, t);
                if (FActive = CWhite) and (y = 2) or (FActive = CBlack) and (y = 7) then
                begin
                  t := f + CVPawn[2] * FActive;
                  AppendMove(f, t, FBoard[t] = CNil);
                end;
              end;
              for i := 3 to 4 do
              begin
                t := f + CVPawn[i] * FActive;
                AppendMove(f, t, (CBlack * FBoard[t] * FActive in [CPawn..CKing]) or (t = FEnPassant[CBlack * FActive]));
              end;
            end;
          CKnight:
            for i := 1 to 8 do
            begin
              t := f + CVKnight[i];
              AppendMove(f, t, CBlack * FBoard[t] * FActive in [CNil..CKing]);
            end;
          CBishop:
            for i := 1 to 4 do
            begin
              t := f;
              repeat
                Inc(t, CVBishop[i]);
                AppendMove(f, t, CBlack * FBoard[t] * FActive in [CNil..CKing]);
              until FBoard[t] <> CNil;
            end;
          CRook:
            for i := 1 to 4 do
            begin
              t := f;
              repeat
                Inc(t, CVRook[i]);
                AppendMove(f, t, CBlack * FBoard[t] * FActive in [CNil..CKing]);
              until FBoard[t] <> 0;
            end;
          CKing:
            for i := 1 to 8 do
            begin
              t := f + CVKing[i];
              AppendMove(f, t, CBlack * FBoard[t] * FActive in [CNil, CPawn, CBishop, CKnight, CRook, CQueen, CKing]);
            end;
          CQueen:
            for i := 1 to 8 do
            begin
              t := f;
              repeat
                Inc(t, CVKing[i]);
                AppendMove(f, t, CBlack * FBoard[t] * FActive in [CNil..CKing]);
              until FBoard[t] <> CNil;
            end;
        end;
      end;
    end;
end;

function TChessPosition.Check: boolean;
var
  i: integer;
begin
  result := FALSE;
  FActive := CBlack * FActive;
  GenerateSimpleMoves;
  FActive := CBlack * FActive;
  for i := 1 to FMoveCount do
    if FBoard[FMoveList[i].FTo] * FActive = CKing then
    begin
      result := TRUE;
      Exit;
    end;
end;

function TChessPosition.CastlingCheck(const AKSquare, AStep: shortint): boolean;
var
  i: integer;
begin
  result := FALSE;
  FActive := CBlack * FActive;
  GenerateSimpleMoves;
  FActive := CBlack * FActive;
  for i := 1 to FMoveCount do
    with FMoveList[i] do
    if (FTo mod 10 = AKSquare mod 10)
    and ((FTo - AKSquare) div AStep >= 0)
    and ((FTo - AKSquare) div AStep <= 2) then
      Exit(TRUE);
end;

procedure TChessPosition.GenerateCastling(const AKSquare, AStep, ARSquare: shortint);
var
  LPos: TChessPosition;
  LSqr: shortint;
begin
  if FBoard[ARSquare] * FActive <> CRook then
    Exit;
  LSqr := AKSquare + AStep;
  repeat
    if FBoard[LSqr] <> CNil then
      Exit;
    Inc(LSqr, AStep);
  until LSqr = ARSquare;
  LPos := TChessPosition.Create;
  LPos.SetPosition(self);
  if not LPos.CastlingCheck(AKSquare, AStep) then
    AppendMove(AKSquare, AKSquare + 2 * AStep);
  LPos.Free;
end;

procedure TChessPosition.GenerateMoves;
begin
  GenerateSimpleMoves;
  if FActive = CWhite then begin
    if FCastling[g1] then GenerateCastling(51, +10, 81);
    if FCastling[c1] then GenerateCastling(51, -10, 11);
  end else begin
    if FCastling[g8] then GenerateCastling(58, +10, 88);
    if FCastling[c8] then GenerateCastling(58, -10, 18);
  end;
end;

var
  LEvalColor, LEvalDepth: integer;

function TChessPosition.BestEval(const AColor, ADepth, AAlpha: integer): integer;
begin
  LEvalColor := AColor;
  LEvalDepth := ADepth - 1;
  result := BestEval(AAlpha);
end;

function TChessPosition.BestEval(const AAlpha: integer): integer;
var
  LValue, i: integer;
  LPos: TChessPosition;
{$IFDEF DEBUG}
  LLine1, LLine2: string;
{$ENDIF}
begin
  Inc(LEvalDepth);
  GenerateMoves;
  i := 0;
  result := -32000 * FActive * LEvalColor;
  LPos := TChessPosition.Create;
{$IFDEF DEBUG}
  LLine1 := EmptyStr;
  LLine2 := EmptyStr;
{$ENDIF}
  while i < FMoveCount do
  begin
    Inc(i);
    LPos.SetPosition(self);
    with LPos.FMoveList[i] do LPos.MovePiece(FFr, FTo, CQueen);
    if (LEvalDepth >= LMinDepth) and (FBoard[FMoveList[i].FTo] = CNil)
    or (LEvalDepth = LMaxDepth)
    or (LTimeLimit > 0) and (GetTickCount64 > LTimeLimit)
    then
      LValue := LPos.Eval(LEvalColor)
    else
      LValue := LPos.BestEval(result);
    FMoveList[i].FVal := LValue;
    if FActive = LEvalColor then
    begin
      if LValue > result then
        result := LValue;
      if (result > AAlpha) or (result = AAlpha) and (LEvalDepth > 2) then
        Break;
    end else
    begin
      if LValue < result then
        result := LValue;
      if (result < AAlpha) or (result = AAlpha) and (LEvalDepth > 2) then
        Break;
    end;
{$IFDEF DEBUG}
    if LEvalDepth = 1 then
      with FMoveList[i] do
      begin
        LLine1 := LLine1 + Format('%6s', [MoveToStr(100 * FFr + FTo)]);
        LLine2 := LLine2 + Format('%6d', [FVal]);
      end;
{$ENDIF}
  end;
{$IFDEF DEBUG}
  if LEvalDepth = 1 then
  begin
    PlayerLogLn(LLine1);
    PlayerLogLn(LLine2);
  end;
{$ENDIF}
  LPos.Free;
  Dec(LEvalDepth);
end;

function TChessPosition.IsLegal(const AMove: integer): boolean;
var
  i: integer;
begin
  result := FALSE;
  i := Low(TMoveList);
  while (i <= FMoveCount) and not result do
    with FMoveList[i] do
      if AMove = 100 * FFr + FTo then
        result := TRUE
      else
        Inc(i);
end;

function TChessPosition.BoardAsText(const APretty: boolean): string;
var
  i: integer;
  LText, LLine: string;
  x, y: integer;
begin
  SetLength(LText, 120);
  for i := 1 to 120 do
    LText[i] := PieceToChar(FBoard[i - 11]);
  
  if APretty then
  begin
    result := '+  a b c d e f g h  +' + LineEnding;
    result := result + '                     ' + LineEnding;
    for y := 8 downto 1 do
    begin
      LLine := Format('%d                   %d', [y, y]);
      for x := 1 to 8 do
        LLine[2 * x + 2] := LText[10 * x + y + 11];
      result := result + LLine + LineEnding;
    end;
    result := result + '                     ' + LineEnding;
    result := result + '+  a b c d e f g h  +';
  end else
    result := LText;
end;

function TChessPosition.GetFen: string;
var
  x, y, n: integer;
  LActiveColor, LCastling, LEnPassant: string;
begin
  result := EmptyStr;
  x := 1;
  y := 8;
  while y >= 1 do
  begin
    if FBoard[10 * x + y] = CNil then
    begin
      n := 0;
      while (x + n <= 8) and (FBoard[10 * (x + n) + y] = CNil) do
        Inc(n);
      result := Concat(result, IntToStr(n));
      Inc(x, n);
    end else
    begin
      result := Concat(result, PieceToChar(FBoard[10 * x + y]));
      Inc(x);
    end;
    if x > 8 then
    begin
      if y > 1 then
        result := Concat(result, '/');
      x := 1;
      Dec(y);
    end;
  end;

  if FActive = CWhite then
    LActiveColor := 'w'
  else
    LActiveColor := 'b';

  LCastling := EmptyStr;
  if FCastling[g1] then LCastling := Concat(LCastling, 'K');
  if FCastling[c1] then LCastling := Concat(LCastling, 'Q');
  if FCastling[g8] then LCastling := Concat(LCastling, 'k');
  if FCastling[c8] then LCastling := Concat(LCastling, 'q');
  if LCastling = EmptyStr then
    LCastling := '-';

  if FEnPassant[FActive] = CNil then
    LEnPassant := '-'
  else
  begin
    LEnPassant := IntToStr(FEnPassant[FActive]);
    LEnPassant[1] := DigitToRow(LEnPassant[1]);
  end;

  result := Format('%s %s %s %s %d %d', [result, LActiveColor, LCastling, LEnPassant, 0, 1]);
end;

end.
