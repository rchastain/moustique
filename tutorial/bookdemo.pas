
uses
  SysUtils, Book;

const
  CStartPosition = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  CBookPath = '../../gm2001.bin';
  
begin
  if FileExists(CBookPath) then
    WriteLn(BestMove(CStartPosition, CBookPath))
  else
    WriteLn('Book not found: ', CBookPath);
end.
