
for program in autoplay bookdemo playercoredemo playerdemo validatordemo
do
  if test -f $program.pas
  then
    echo Compile $program.pas
    fpc -B -Mobjfpc -Sh -Fu.. -FU. $program.pas -dDEBUG
  else
    echo File not found: $program.pas
  fi
done

rm -f *.o
rm -f *.ppu
