
uses
  SysUtils, Classes, Validator;

{$I samples.pas}

var
  LValidator: TFenValidator;

var
  LMoves: TStringList;
  i: integer;
  
begin
  LValidator := TFenValidator.Create;
  
  for i := Low(CFenSample) to High(CFenSample) do
    WriteLn(LValidator.IsFEN(CFenSample[i]));
  
  LMoves := TStringList.Create;
  
  if LValidator.ExtractMoves(CUciSample[3], LMoves) then
    WriteLn(LMoves.Text);
  
  LMoves.Free;
  LValidator.Free;
end.
