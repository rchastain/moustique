
# Moustique (JSSchach UCI)

## Overview

Jürgen Schlottke's didactic chess program, with a minimalist UCI interface.

## Build

To build *Moustique*, you need the Free Pascal compiler.

You can use the following command line:

    fpc -Mobjfpc -Sh moustique.pas -dRELEASE -CX -XX -Xs

If you prefer, you can use instead the Lazarus project (*moustique.lpi*) or the MSEgui project (*moustique.prj*) included in the repository.

## Book

Since version 0.3, *Moustique* is able to use a Polyglot opening book.

By default, *Moustique* uses the small [Polyglot book](http://www.open-aurec.com/chesswar/download.html) by Olivier Deville.

## Logo

![alt text](https://gitlab.com/rchastain/moustique/-/raw/master/logo/Farman%20F455%20Moustique.bmp)
