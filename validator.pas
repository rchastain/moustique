
unit Validator;

interface

uses
  SysUtils, Classes, RegExpr;

type
  TFenValidator = class
    private
      LExprDigit,
      LExprFen,
      LExprMove,
      LExprPieces,
      LExprActive,
      LExprCastling,
      LExprEnPassant,
      LExprHalfMove,
      LExprFullMove: TRegExpr;
      function ExpandEmptySquares(AExpr: TRegExpr): string;
      function PatternFen: string;
    public
      constructor Create;
      destructor Destroy; override;
      function IsFen(const AStr: string): boolean;
      function ExtractFen(const AStr: string; var AFen: string): boolean;
      function ExtractMoves(const AStr: string; const AList: TStringList): boolean;
  end;
  
implementation

function TFenValidator.ExpandEmptySquares(AExpr: TRegExpr): string;
begin
  result := StringOfChar('-', StrToInt(AExpr.Match[0]));
end;

function TFenValidator.PatternFen: string;
const
  CPatternPieces    = '[1-8BKNPQRbknpqr]{1,8}';
  CPatternActive    = '[wb]';
  CPatternCastling  = '([KQkq]{1,4}|\-)';
  CPatternEnPassant = '([a-h][36]|\-)';
  CPatternNumber    = '\d+';
var
  LPatternPiecePlacement: string;
begin
  LPatternPiecePlacement := ReplaceRegExpr('x', 'x/x/x/x/x/x/x/x', CPatternPieces, FALSE);
  result := Format('%s %s %s %s %s %s', [LPatternPiecePlacement, CPatternActive, CPatternCastling, CPatternEnPassant, CPatternNumber, CPatternNumber]);
end;

constructor TFenValidator.Create;
const
  CPatternMove            = '\b[a-h][1-8][a-h][1-8][nbrq]?\b';
  CPatternPiecesStrict    = '^[1-8BKNPQRbknpqr]+$';
  CPatternActiveStrict    = '^[wb]$';
  CPatternCastlingStrict  = '^[KQkq]+$|^[A-Ha-h]+$|^\-$';
  CPatternEnPassantStrict = '^[a-h][36]$|^\-$';
  CPatternHalfMoveStrict  = '^\d+$';
  CPatternFullMoveStrict  = '^[1-9]\d*$';
begin
  inherited;
  LExprDigit     := TRegExpr.Create('\d');
  LExprFen       := TRegExpr.Create(PatternFen);
  LExprMove      := TRegExpr.Create(CPatternMove);
  LExprPieces    := TRegExpr.Create(CPatternPiecesStrict);
  LExprActive    := TRegExpr.Create(CPatternActiveStrict);
  LExprCastling  := TRegExpr.Create(CPatternCastlingStrict);
  LExprEnPassant := TRegExpr.Create(CPatternEnPassantStrict);
  LExprHalfMove  := TRegExpr.Create(CPatternHalfMoveStrict);
  LExprFullMove  := TRegExpr.Create(CPatternFullMoveStrict);
end;

destructor TFenValidator.Destroy;
begin
  LExprDigit.Free;
  LExprFen.Free;
  LExprMove.Free;
  LExprPieces.Free;
  LExprActive.Free;
  LExprCastling.Free;
  LExprEnPassant.Free;
  LExprHalfMove.Free;
  LExprFullMove.Free;
  inherited;
end;

function TFenValidator.IsFen(const AStr: string): boolean;
var
  LFields, LRanks: TStringList;
  LRank: string;
  i: integer;
begin
  LFields := TStringList.Create;
  LRanks := TStringList.Create;

  ExtractStrings([' '], [], PChar(AStr), LFields);
  result := LFields.Count = 6;

  if result then
  begin
    ExtractStrings(['/'], [], PChar(LFields[0]), LRanks);
    result := LRanks.Count = 8;
  end;
  
  if result then
  begin
    result := result and (Pos('K', LFields[0]) > 0);
    result := result and (Pos('k', LFields[0]) > 0);

    for i := 0 to 7 do
    begin
      result := result and LExprPieces.Exec(LRanks[i]);
      if result then
      begin
        LRank := LRanks[i];
        while LExprDigit.Exec(LRank) do
          LRank := LExprDigit.Replace(LRank, @ExpandEmptySquares);
        result := result and (Length(LRank) = 8);
      end;
    end;
    
    result := result and LExprActive.Exec   (LFields[1]);
    result := result and LExprCastling.Exec (LFields[2]);
    result := result and LExprEnPassant.Exec(LFields[3]);
    result := result and LExprHalfMove.Exec (LFields[4]);
    result := result and LExprFullMove.Exec (LFields[5]);
  end;

  LFields.Free;
  LRanks.Free;
end;

function TFenValidator.ExtractFen(const AStr: string; var AFen: string): boolean;
begin
  result := LExprFen.Exec(AStr);
  if result then
    AFen := LExprFen.Match[0]
  else
    AFen := EmptyStr;
end;

function TFenValidator.ExtractMoves(const AStr: string; const AList: TStringList): boolean;
begin
  result := LExprMove.Exec(AStr);
  if result then 
  begin
    AList.Clear;
    repeat 
      AList.Append(LExprMove.Match[0]);
    until not LExprMove.ExecNext;
  end;
end;

end.
