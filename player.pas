
{ Autor:   Jürgen Schlottke, Schönaich-C.-Str. 46, D-W 2200 Elmshorn
           Tel. 04121/63109
  Zweck  : Demonstration der Schachprogrammierung unter Turbo-Pascal
  Datum  : irgendwann 1991, als PD freigegeben am 18.01.93
  Version: ohne Versionsnummer
}

unit Player;

interface

uses
  PlayerCore;

const
  CStartPos = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  
type
  TExitCode = (ecSuccess, ecCheck, ecCheckmate, ecStalemate, ecError);
  
  TChessPlayer = class
  strict private
    FIniPos,
    FCurPos: TChessPosition;
    FMoveCount: integer;
    FList: TMoveList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetPosition(const AFen: string);
    function PlayMove(const AMove: string): boolean;
    function BestMoveIndex(const ABestValue: integer): integer;
    function BestMove(out ACode: TExitCode): string; overload;
    function BestMove: string; overload;
    function BoardAsText(const APretty: boolean = TRUE): string;
    function GetFen: string;
    function SetPosition(const AFen: string; const AMoves: array of string): string;
    function WhiteToMove: boolean;
  end;

implementation

uses
  Classes, SysUtils, TypInfo, PlayerTypes{$IFDEF DEBUG}, Log{$ENDIF};

constructor TChessPlayer.Create;
begin
  inherited Create;
  FIniPos := TChessPosition.Create(CStartPos);
  FCurPos := TChessPosition.Create;
end;

destructor TChessPlayer.Destroy;
begin
  FCurPos.Free;
  FIniPos.Free;
  inherited Destroy;
end;

procedure TChessPlayer.SetPosition(const AFen: string);

  function GetMoveCount(const AFen: string): integer;
  var
    LFields: TStringList;
  begin
    LFields := TStringList.Create;
    ExtractStrings([' '], [], pchar(AFen), LFields);
    if LFields.Count = 6 then
      result := 2 * Pred(StrToInt(LFields[5])) + Ord(LFields[1] = 'b')
    else
      result := 0;
    LFields.Free;
  end;

begin
  FCurPos.SetPosition(AFen);
  FMoveCount := GetMoveCount(AFen);
end;

function TChessPlayer.PlayMove(const AMove: string): boolean;
var
  LPos: TChessPosition;
  LMove, LPr, LFr, LTo: integer;
begin
  MoveToInt(AMove, LMove, LPr);
  LFr := LMove div 100;
  LTo := LMove mod 100;
  FCurPos.GenerateMoves;
  result := FCurPos.IsLegal(LMove);
  if result then
  begin
    LPos := TChessPosition.Create;
    LPos.SetPosition(FCurPos);
    LPos.MovePiece(LFr, LTo, LPr);
    LPos.ActiveColor := CBlack * LPos.ActiveColor;
    if LPos.Check then
      result := FALSE
    else
    begin
      FCurPos.MovePiece(LFr, LTo, LPr);
      Inc(FMoveCount);
    end;
    LPos.Free;
  end;
end;

function TChessPlayer.BestMoveIndex(const ABestValue: integer): integer;
var
  LPos: TChessPosition;
  LMaxValue: integer;
  LCount: integer;
  i, j: integer;
{$IFDEF DEBUG}
  LLine1, LLine2: string;
{$ENDIF}
begin
  LPos := TChessPosition.Create;

  with FCurPos do
  begin
    LCount := 0;
    for i := 1 to MoveCount do
      if MoveList[i].FVal = ABestValue then
      begin
        Inc(LCount);
        FList[LCount].FFr := MoveList[i].FFr;
        FList[LCount].FTo := MoveList[i].FTo;
        FList[LCount].FVal := 0
      end;
  end;

  LMaxValue := Low(integer);
  result := 0;
{$IFDEF DEBUG}
  LLine1 := EmptyStr;
  LLine2 := EmptyStr;
{$ENDIF}
  for i := 1 to LCount do
  begin
    LPos.SetPosition(FCurPos);

    with LPos do
    begin
      if Board[FList[i].FFr] = FIniPos.Board[FList[i].FFr] then
      begin
        Inc(FList[i].FVal, 5);
        if Board[FList[i].FFr] * ActiveColor = CPawn then
          Inc(FList[i].FVal, 2);
      end;

      if Board[FList[i].FFr] * ActiveColor = CKing then
        if Abs(FList[i].FTo - FList[i].FFr) = 20 then
          Inc(FList[i].FVal, 20)
        else
          Dec(FList[i].FVal, 10);

      if (FMoveCount < 32) and (Board[FList[i].FFr] * ActiveColor in [CPawn, CBishop, CKnight]) then
        Inc(FList[i].FVal, 20);

      if (FList[i].FFr div 10 = 1)
      or (FList[i].FFr div 10 = 8)
      or (FList[i].FFr mod 10 = 1)
      or (FList[i].FFr mod 10 = 8) then
        Inc(FList[i].FVal, 2);

      if (FList[i].FTo div 10 = 1)
      or (FList[i].FTo div 10 = 8)
      or (FList[i].FTo mod 10 = 1)
      or (FList[i].FTo mod 10 = 8) then
        Dec(FList[i].FVal, 2);
    end;

    LPos.SetPosition(FCurPos);
    LPos.MovePiece(FList[i].FFr, FList[i].FTo, CQueen);

    if LPos.Board[FList[i].FTo] = FIniPos.Board[FList[i].FTo] then
      Dec(FList[i].FVal, 10);

    LPos.ActiveColor := CBlack * LPos.ActiveColor;
    LPos.GenerateSimpleMoves;

    with LPos do
      for j := 1 to MoveCount do
      begin
        Inc(FList[i].FVal);
        if Board[MoveList[j].FTo] <> CNil then
          Inc(FList[i].FVal);
      end;

    LPos.ActiveColor := CBlack * LPos.ActiveColor;
    LPos.GenerateSimpleMoves;

    with LPos do
      for j := 1 to MoveCount do
      begin
        Dec(FList[i].FVal);
        if Board[MoveList[j].FTo] <> CNil then
          Dec(FList[i].FVal);
      end;
{$IFDEF DEBUG}
    with FList[i] do
    begin
      LLine1 := LLine1 + Format('%6s', [MoveToStr(100 * FFr + FTo)]);
      LLine2 := LLine2 + Format('%6d', [FVal]);
    end;
{$ENDIF}
    if FList[i].FVal >= LMaxValue then
    begin
      LMaxValue := FList[i].FVal;
      result := i;
    end;
  end;
{$IFDEF DEBUG}
  PlayerLogLn(LLine1);
  PlayerLogLn(LLine2);
{$ENDIF}
  LPos.Free;
end;

function TChessPlayer.BestMove(out ACode: TExitCode): string;
const
  CCode: array[boolean, boolean] of TExitCode = ((ecStalemate, ecCheckmate), (ecSuccess, ecCheck));
var
  LCheckBefore, LPr: boolean;
  i: integer;
begin
  result := '0000';
  i := FCurPos.BestEval(FCurPos.ActiveColor, 1, 32000);
  i := BestMoveIndex(i);
  if i > 0 then
  begin
    Inc(FMoveCount);
    LCheckBefore := FCurPos.Check;
    with FList[i] do
      LPr := FCurPos.MovePiece(FFr, FTo, CQueen);
    FCurPos.ActiveColor := CBlack * FCurPos.ActiveColor;
    if FCurPos.Check then
      ACode := CCode[FALSE, LCheckBefore]
    else
    begin
      with FList[i] do
        result := MoveToStr(100 * FFr + FTo);
      if LPr then
        result := result + 'q';
      FCurPos.ActiveColor := CBlack * FCurPos.ActiveColor;
      ACode := CCode[TRUE, FCurPos.Check];
    end;
  end else
    ACode := ecError;
{$IFDEF DEBUG}
  if not (ACode in [ecSuccess, ecCheck]) then
    PlayerLogLn(Format('bestmove exit code %d', [Ord(ACode)]));
{$ENDIF}
end;

function TChessPlayer.BestMove: string;
var
  LCode: TExitCode;
begin
  result := BestMove(LCode);
end;

function TChessPlayer.BoardAsText(const APretty: boolean): string;
begin
  result := FCurPos.BoardAsText(APretty);
end;

function TChessPlayer.GetFen: string;
begin
  result := FCurPos.GetFen;
end;

function TChessPlayer.SetPosition(const AFen: string; const AMoves: array of string): string;
var
  i: integer;
begin
  SetPosition(AFen);
  for i := Low(AMoves) to High(AMoves) do
    PlayMove(AMoves[i]);
  result := GetFen;
end;

function TChessPlayer.WhiteToMove: boolean;
begin
  result := FCurPos.ActiveColor = CWhite;
end;

end.
