
unit Uci;

interface

uses
  Classes, Validator;

type
  TUciCommand =
  (
    cmdUci,
    cmdQuit,
    cmdNewGame,
    cmdPositionFen,
    cmdPositionStartPos,
    cmdGo,
    cmdIsReady,
    cmdStop,
    cmdUnknown
  );

  TUciCommandParser = class
    private
      LValidator: TFenValidator;
      FMoves: TStringList;
      FPosition: string;
      FMoveTime, FWTime, FBTime, FMovesToGo, FWInc, FBInc: integer;
    public
      constructor Create;
      destructor Destroy; override;
      function ParseCommand(const ACommand: string): TUciCommand;
      property Moves: TStringList read FMoves;
      property Position: string read FPosition;
      property MoveTime: integer read FMoveTime;
      property WTime: integer read FWTime;
      property BTime: integer read FBTime;
      property MovesToGo: integer read FMovesToGo;
      property WInc: integer read FWInc;
      property BInc: integer read FBInc;
  end;

implementation

uses
  SysUtils, StrUtils;
  
procedure ExtractGoParameters(const ACommand: string; out AMoveTime, AWTime, ABTime, AMovesToGo, AWInc, ABInc: integer);
{
  go movetime 500
  go wtime 600000 btime 600000
  go wtime 59559 btime 56064 movestogo 38
  go wtime 60000 btime 60000 winc 1000 binc 1000
  go wtime 2039 winc 20 btime 1690 binc 20
}
var
  i: integer;
  s: string;
begin
  AMoveTime  := -2;
  AWTime     := -2;
  ABTime     := -2;
  AMovesToGo := -2;
  AWInc      := -2;
  ABInc      := -2;
  for i := 1 to WordCount(ACommand, [' ']) do
  begin
    s := ExtractWord(i, ACommand, [' ']);
    case s of
      'movetime':  AMoveTime  := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
      'wtime':     AWTime     := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
      'btime':     ABTime     := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
      'movestogo': AMovesToGo := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
      'winc':      AWInc      := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
      'binc':      ABInc      := StrToIntDef(ExtractWord(Succ(i), ACommand, [' ']), -1);
    end;
  end;
end;

constructor TUciCommandParser.Create;
begin
  LValidator := TFenValidator.Create;
  FMoves     := TStringList.Create;
  FPosition  := EmptyStr;
  FMoveTime  := -3;
  FWTime     := -3;
  FBTime     := -3;
  FMovesToGo := -3;
  FWInc      := -3;
  FBInc      := -3;
end;

destructor TUciCommandParser.Destroy;
begin
  LValidator.Free;
  FMoves.Free;
  inherited Destroy;
end;

function TUciCommandParser.ParseCommand(const ACommand: string): TUciCommand;
begin
  if ACommand = 'uci' then
    result := cmdUci
  else
    if ACommand = 'quit' then
      result := cmdQuit
    else
      if ACommand = 'ucinewgame' then
        result := cmdNewGame
      else
        if (Pos('position fen', ACommand) = 1)
        and LValidator.ExtractFen(ACommand, FPosition) then
        begin
          result := cmdPositionFen;
          LValidator.ExtractMoves(ACommand, FMoves);
        end else
          if (Pos('position startpos', ACommand) = 1) then
          begin
            result := cmdPositionStartPos;
            LValidator.ExtractMoves(ACommand, FMoves);
          end else
            if Pos('go', ACommand) = 1 then
            begin
              result := cmdGo;
              ExtractGoParameters(ACommand, FMoveTime, FWTime, FBTime, FMovesToGo, FWInc, FBInc);
            end else
              if ACommand = 'isready' then
                result := cmdIsReady
              else
                if ACommand = 'stop' then
                  result := cmdStop
                else
                  result := cmdUnknown;
end;

end.
