
unit Log;

interface

uses
  SysUtils;

procedure LogLn(const AText: string);
procedure PlayerLogLn(const AText: string);

implementation

var
  LMainLog, LPlayerLog: text;

procedure LogLn(const AText: string);
begin
  WriteLn(LMainLog, DateTimeToStr(Now) + ' ' + AText);
  Flush(LMainLog);
end;

procedure PlayerLogLn(const AText: string);
begin
  WriteLn(LPlayerLog, AText);
  Flush(LPlayerLog);
end;

var
  LFileName: string;
  
initialization
  LFileName := ChangeFileExt(ParamStr(0), '.log');
  Assign(LMainLog, LFileName);
  if FileExists(LFileName) then
    Append(LMainLog)
  else
    Rewrite(LMainLog);
{$IFDEF DEBUG}
  Insert('.player', LFileName, Pos('.log', LFileName));
  Assign(LPlayerLog, LFileName);
  if FileExists(LFileName) then
    Append(LPlayerLog)
  else
    Rewrite(LPlayerLog);
{$ENDIF}
finalization
  Close(LMainLog);
{$IFDEF DEBUG}
  Close(LPlayerLog);
{$ENDIF}
end.
