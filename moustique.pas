
program Moustique;

uses
{$IFDEF UNIX}
  CThreads, CWString,
{$ENDIF}
  Classes, SysUtils, Player, Uci, Validator, Settings, Book, Log, Global;

{$INCLUDE version.pas}

var
  LChessPlayer: TChessPlayer;
  LValidator: TFenValidator;
  LParser: TUciCommandParser;
  
function GetPlayerMove(const AFen: string; const AMoves: array of string): string;
var
  LFen: string;
begin
  if not LValidator.IsFen(AFen) then
  begin
    LogLn(Format('invalid fen [%s]', [AFen]));
    Exit('0001');
  end;
  LFen := LChessPlayer.SetPosition(AFen, AMoves);
  result := Book.BestMove(LFen, LBook);
  if result <> EmptyStr then
    LogLn(Format('book move %s', [result]))
  else
    result := LChessPlayer.BestMove;
end;

var
  LPosition: string;
  LMovesArray: array of string;

type
  TBestMoveThread = class(TThread)
  protected
    procedure Execute; override;
  end;

procedure TBestMoveThread.Execute;
var
  LMove: string;
  LMoveTime: integer;
  LTime: qword;
begin
  LTime := GetTickCount64;
  
  with LParser do
    if MoveTime > 0 then
      LMoveTime := MoveTime
    else
      if MovesToGo > 0 then
        if LChessPlayer.WhiteToMove then
          LMoveTime := WTime div MovesToGo
        else
          LMoveTime := BTime div MovesToGo
      else
        if WInc > 0 then
          if LChessPlayer.WhiteToMove then
            LMoveTime := WInc + WTime div 50
          else
            LMoveTime := BInc + BTime div 50
        else
          LMoveTime := 1000;
  
  LogLn(Format('time allowed %d ms', [LMoveTime]));
  LTimeLimit := LTime + LMoveTime;
  
  LMove := GetPlayerMove(LPosition, LMovesArray);
  SetLength(LMovesArray, 0);
  WriteLn(output, Format('bestmove %s', [LMove]));
  Flush(output);
  
  LTime := GetTickCount64 - LTime;
  LogLn(Format('execution time %d ms', [LTime]));
end;

var
  LCommand: string;
  LUciCommand: TUciCommand;
  LThread: TBestMoveThread;
  LIndex: integer;

begin
  Randomize;
  
  LogLn(Format('Moustique %s %s %s Free Pascal %s', [CAppVersion, {$I %DATE%}, {$I %TIME%}, {$I %FPCVERSION%}]));
  
  LChessPlayer := TChessPlayer.Create;
  LParser := TUciCommandParser.Create;
  LValidator := TFenValidator.Create;
  LPosition := CStartPos;
  SetLength(LMovesArray, 0);

  while not Eof do
  begin
    ReadLn(input, LCommand);
    LogLn('> ' + LCommand);

    LUciCommand := LParser.ParseCommand(LCommand);
    case LUciCommand of
      cmdUci:
        begin
          WriteLn(output, 'id name ' + CAppName + ' ' + CAppVersion);
          WriteLn(output, 'id author ' + CAppAuthor);
          WriteLn(output, 'uciok');
          Flush(output);
        end;
      cmdQuit:
        Break;
      cmdNewGame:
        LPosition := CStartPos;
      cmdPositionFen:
        begin
          LPosition := LParser.Position;
          SetLength(LMovesArray, LParser.Moves.Count);
          for LIndex := 0 to Pred(LParser.Moves.Count) do
            LMovesArray[LIndex] := LParser.Moves[LIndex];
        end;
      cmdPositionStartPos:
        begin
          LPosition := CStartPos;
          SetLength(LMovesArray, LParser.Moves.Count);
          for LIndex := 0 to Pred(LParser.Moves.Count) do
            LMovesArray[LIndex] := LParser.Moves[LIndex];
        end;
      cmdGo:
        begin
          LThread := TBestMoveThread.Create(TRUE);
          LThread.FreeOnTerminate := TRUE;
          LThread.Priority := {tpHigher}tpNormal;
          LThread.Start;
        end;
      cmdIsReady:
        begin
          WriteLn(output, 'readyok');
          Flush(output);
        end;
      cmdStop:
        begin
          WriteLn(output, 'bestmove 0002');
          Flush(output);
        end;
      cmdUnknown:
        begin
          LogLn(Format('Unknown command: %s', [LCommand]));
        end;
    end;
  end;
  
  LChessPlayer.Free;
  LParser.Free;
  LValidator.Free;
end.
